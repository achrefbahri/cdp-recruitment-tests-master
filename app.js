const { filterData, countPeopleAnimals } = require("./scriptsData");
const util = require("util");

/* extract args from command bash*/
const args = process.argv.slice(2);

if (args.length > 0) {
  const [op, filter] = args[0].split("=");
  /*call function switch args to find data*/
  if (op === "--filter")
    console.log(util.inspect(filterData(filter), false, null, true));
  else if (op === "--count")
    console.log(util.inspect(countPeopleAnimals(filter), false, null, true));
  else console.error("error:command bash");
} else console.error("error:command bash");
