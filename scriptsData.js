const { data } = require("./data");

/* find Elements in array by filter */

const filterArray=(animals,filter)=>animals.filter((animal) =>
animal.name.includes(filter)
);


/*filter array by filter */

const filterData = (filter) => {
  var result = [];
  data.map(({ name, people }) => {
    var listAnimals = [];
    var peoples = [];
    people.map((item) => {
      listAnimals = filterArray(item.animals,filter)
      if (listAnimals.length > 0) {
        const itemPeoples = {
          name: item.name,
          animals: listAnimals,
        };
        peoples = [...peoples, itemPeoples];
      }
    });
    if (peoples.length > 0) result = [...result, { name, people: peoples }];
  });
  return result;
};

/*filter array by filter animal and count  */

const countPeopleAnimals = (filter="ry") => {
  var result = [];
  data.map(({ name, people }) => {
    var listAnimals = [];
    var peoples = [];
    people.map((item) => {
      /* find animals by filter  */
      listAnimals = filterArray(item.animals,filter)

      if (listAnimals.length > 0) {
        const itemPeoples = {
          name: `${item.name} [${listAnimals.length}]`,
          animals: listAnimals,
        };
        peoples = [...peoples, itemPeoples];
      }
    });
    if (peoples.length > 0)
      result = [
        ...result,
        { name: `${name} [${peoples.length}]`, people: peoples },
      ];
  });
  return result;
};

module.exports = {
  filterData,
  countPeopleAnimals,
};
